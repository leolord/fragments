requirejs.config({
  baseUrl: '<%page%>',
  packages: [
    {
      name: 'f',
      location: '../../com'
    },
    {
      name: 't',
      location: '../third'
    }
  ],
  map: {
    '*' : {
      'css'        : 't/require-css/require-css',
      'star-score' : 'f/star-score/star-score'
    }
  },
  paths: {
    'jquery'     : '../third/jquery.min',
    'text'       : '../third/require-text',
    'underscore' : '../third/underscore-min',
    'bootstrap'  : '../third/bootstrap.min',
    'tpl'        : '../third/tpl'
  },
  shim: {
    'jquery': {
      exports: '$'
    },
    'underscore' : {
      deps: ['jquery'],
      exports: '_'
    },
    'bootstrap' : {
      deps: ['jquery']
    }
  }
});

