//判断某个对象是否是一个类数组对象
define([], function(){
  return function(a){
    return a !== undefined &&
           a !== null &&
           a.length !== undefined &&
           typeof a.length === 'number';
  };
});
