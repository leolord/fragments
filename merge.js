define(['./likeArray', './mixin'], function(likeArray, Mixin){
  return function(a, b){
    var result;
    if(likeArray(a)){
      result = [];
    } else {
      result = {};
    }

    Mixin(result, a);
    Mixin(result, b);

    return result;
  };
});

