# 此项目已经废弃
___
#一些常用的JS脚本

## 开发目的
一些常用的js脚本，采用AMD格式开发，仅仅为了实现简单的常用的功能，不求兼容性，不要求大而全，够用就行。  
由于我目前在淘宝，经常使用KISSY框架（脱胎与YUI），所以会将KISSY中常用的功能简化，并放到这个仓库里面。  
避免重复发明轮子，所以jquery（或者zepto）和underscore包含的功能不再重复开发。  

## 必须使用脚手架
由于目前条件限制，还没办法将这个库用于任意目录结构下面，所以暂时只能使用特定的脚手架工具：http://git.oschina.net/leolord/fragments-builder  
这个脚手架工具马上会升级成顶级仓库，而不是ProjectTemplate下的一个目录

## 目前包含的功能

*   **base.js**         实现简单的继承、事件、属性机制。
*   **camel.js**        将字符串转换成camel格式
*   **classEvent.js**   base.js的依赖，在一个类的prototype中注入一些方法，使之支持事件
*   **likeArray.js**    判断某对象是否是数组，或者类似数组
*   **merge.js**        合并两个对象到新的对象（或者数组），只支持浅拷贝
*   **mixin.js**        合并两个对象或者数组（到第一个参数中），支持浅拷贝和有深度限制的深拷贝
*   **uri.js**          见到那的uri分析类，用于检测url中包含的query参数
*   **thinkphp.js**     对于thinkphp的路径进行简单的封装，这是由于不同的服务器支持的thinkphp的路由方式不同，可能我们的脚本要部署到不支持simple方式路由的服务器上，而我们有没办法修改服务器，所以添加了这个脚本
*   **star-score/star-score**  这个是一个评分的组件，包含UI，可以设定允许用户点击评分或者只读。
*   **rem.less**        延续淘宝对于移动页面的处理方式，将页面按照宽度分成16份，没份宽度未1rem。16这个magic number是由来自淘宝的mbase.js保证的。rem.less可以让我编写网页的时候，直接使用视觉稿中得单位，例如：   
  ```less
    @screenWidth:640;
    @rem-fixed:4;
    .main{
      .rem(width, 140);
    }
  ```   
  将会输出   
  ```css
    .main{
      width: 3.5rem;//至多保留4位小数
    }
  ```

## TODO
* 项目编译优化，将jquery等常用的库剥离开，不参加optimizing。
* config.js优化，摆脱目录结构的限制。

## 关于config.js
编写常用的requirejs的配置文件，这个属于重复劳动，所以将配置文件写成一个模板，当然这个模板是有限制的，其中packages字段中的location都是相对路径，这是由于目前这个算不上框架的框架还没有一个网上的绝对地址，只能用相对地址。  
而相对地址也会带来问题，那就是工程目录的结构必须定死。

___