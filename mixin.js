define(['./likeArray'], function(likeArray) {
  var mixin = function(a, b, depth) {
    var result;
    if (likeArray(a)) {
      for (var i = 0; i < b.length; ++i) {
        a[i + a.length] = b[i];
      }
    } else {
      var k;

      for (k in b) {
        if (depth) {
          a[k] = {};
          mixin(a[k], b[k], depth-1);
        } else {
          a[k] = b[k];
        }
      }
    }
    return a;
  };
  return mixin;
});

