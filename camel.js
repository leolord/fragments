// 将字符串转换为驼峰格式
//Note: 未测试
define([], function(){
  /**
   * @module Fragments
   * @method Camel
   * @param {String} str 需要转换成驼峰格式的字符串
   * @param {Boolean} [firstLetterLowercase=true] 是否首字母小写，默认小写
   * */
  return function(str, firstLetterLowercase){
    var slices = str.split('-');

     firstLetterLowercase = (firstLetterLowercase === undefined || firstLetterLowercase === true);

    for(var i = 0;  i < slices.length; ++i){
      var slice = slices[i];

      if(firstLetterLowercase && i === 0){
        slices[i] = slice[0].toLowerCase() + slice.slice(1);
      } else {
        slices[i] = slice[0].toUpperCase() + slice.slice(1);
      }
    }
    return slices.join('');
  };
});
