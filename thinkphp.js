//thinkphp的URL模式的封装，由于经常要在不同环境下切换，而nginx的配置不是很友好，所以用前端的办法解决
define([], function() {
  var PATHINFO   = 'pathinfo';
  var NORMAL     = 'normal';
  var REWRITE    = 'rewrite';
  var COMPATIBLE = 'compatible';

  return function(host, module, controller, action, mode) {
    var mode = mode || PATHINFO;
    switch (mode) {
      case PATHINFO:
        return host + 'index.php/' + module + '/' + controller + '/' + action;
      case NORMAL:
        return host + 'index.php?m=' + module + '&c=' + controller + '&a=' + action;
      case REWRITE:
        return host + module + '/' + controller + '/' + action;
      case COMPATIBLE:
        return host + '?s=' + module + '/' + controller + '/' + action;
    }
  }
});

